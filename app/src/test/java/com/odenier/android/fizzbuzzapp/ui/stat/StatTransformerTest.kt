package com.odenier.android.fizzbuzzapp.ui.stat

import com.odenier.android.fizzbuzzapp.ui.form.FormModel
import org.junit.Assert.assertEquals
import org.junit.Test

class StatTransformerTest {

    @Test
    fun should_return_empty_result_with_empty_data_list() {
        val transformer = StatTransformer(arrayListOf())
        val result = transformer.transform()
        assertEquals(0, result.size)
    }

    @Test
    fun should_return_one_result_with_only_one_request() {
        val data = arrayListOf(FormModel())
        val transformer = StatTransformer(data)
        val result = transformer.transform()
        assertEquals(1, result.size)
        assertEquals("0-0", result[0].first)
        assertEquals(1, result[0].second)
    }

    @Test
    fun should_return_one_result_with_many_same_request() {
        val data = arrayListOf(FormModel(), FormModel(), FormModel())
        val transformer = StatTransformer(data)
        val result = transformer.transform()
        assertEquals(1, result.size)
        assertEquals("0-0", result[0].first)
        assertEquals(3, result[0].second)
    }

    @Test
    fun should_return_sorted_descending_result_with_many_different_request() {
        val data = arrayListOf(
            FormModel(1,"a", 2, "b", 10),
            FormModel(3,"fizz", 5, "buzz", 100),
            FormModel(1,"aa", 2, "bb", 20),
            FormModel())
        val transformer = StatTransformer(data)
        val result = transformer.transform()
        assertEquals(3, result.size)
        assertEquals("1-2", result[0].first)
        assertEquals(2, result[0].second)
        assertEquals("0-0", result[1].first)
        assertEquals(1, result[1].second)
        assertEquals("3-5", result[2].first)
        assertEquals(1, result[2].second)
    }
}