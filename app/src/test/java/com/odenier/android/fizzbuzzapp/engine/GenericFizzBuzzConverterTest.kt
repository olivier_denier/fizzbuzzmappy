package com.odenier.android.fizzbuzzapp.engine

import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test

class GenericFizzBuzzConverterTest {

    @Test
    fun should_check_convert_without_rule() {
        val converter = GenericFizzBuzzConverter(100)

        assertEquals("", converter.convert(0))
        for (i in 1..100) {
            assertEquals(i.toString(), converter.convert(i))
        }
    }

    @Test
    fun should_check_convert_with_one_rule() {
        val converter = GenericFizzBuzzConverter(15)
        converter.addRule(FizzBuzzRule(3, "fizz"))

        assertEquals("", converter.convert(0))
        checkClassicalNumbers(converter)
        assertEquals("fizz", converter.convert(3))
        assertEquals("fizz", converter.convert(6))
        assertEquals("fizz", converter.convert(9))
        assertEquals("fizz", converter.convert(12))
        assertEquals("fizz", converter.convert(15))
    }

    @Test
    fun should_check_convert_with_two_rules() {
        val converter = GenericFizzBuzzConverter(15)
        converter.addRule(FizzBuzzRule(3, "fizz"))
        converter.addRule(FizzBuzzRule(5, "buzz"))

        assertEquals("", converter.convert(0))
        checkClassicalNumbers(converter)
        assertEquals("fizz", converter.convert(3))
        assertEquals("buzz", converter.convert(5))
        assertEquals("fizz", converter.convert(6))
        assertEquals("fizz", converter.convert(9))
        assertEquals("buzz", converter.convert(10))
        assertEquals("fizz", converter.convert(12))
        assertEquals("fizzbuzz", converter.convert(15))
    }

    @Test
    fun should_check_convert_with_three_rules() {
        val converter = GenericFizzBuzzConverter(100)
        converter.addRule(FizzBuzzRule(3, "fizz"))
        converter.addRule(FizzBuzzRule(5, "buzz"))
        converter.addRule(FizzBuzzRule(15, "foozz"))

        assertEquals("", converter.convert(0))
        checkClassicalNumbers(converter)
        for (number in 3..100 step 3) {
            assertTrue(converter.convert(number).contains("fizz"))
        }
        for (number in 5..100 step 5) {
            assertTrue(converter.convert(number).contains("buzz"))
        }
        for (number in 15..100 step 15) {
            assertTrue(converter.convert(number).contains("foozz"))
        }
    }

    private fun checkClassicalNumbers(converter: GenericFizzBuzzConverter) {
        for (number in 1..converter.getLimit()) {
            for (rule in converter.rules) {
                if (number == rule.intValue) return
            }
            assertEquals(number.toString(), converter.convert(number))
        }
    }
}