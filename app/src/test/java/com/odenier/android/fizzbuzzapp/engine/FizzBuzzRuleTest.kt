package com.odenier.android.fizzbuzzapp.engine

import org.junit.Assert.assertEquals
import org.junit.Test

class FizzBuzzRuleTest {

    @Test
    fun compute_method_should_return_empty_string_if_intValue_equals_zero() {
        val rule = FizzBuzzRule(0, "")
        assertEquals("", rule.compute(0))
        assertEquals("", rule.compute(1))
        assertEquals("", rule.compute(100))
    }

    @Test
    fun compute_method_should_return_stringValue_if_intValue_equals_one() {
        val rule = FizzBuzzRule(1, "ok")
        assertEquals("ok", rule.compute(0))
        assertEquals("ok", rule.compute(1))
        assertEquals("ok", rule.compute(100))
    }

    @Test
    fun compute_method_should_return_stringValue_if_intValue_equals_two() {
        val rule = FizzBuzzRule(2, "ok")
        assertEquals("ok", rule.compute(0))
        assertEquals("", rule.compute(1))
        assertEquals("ok", rule.compute(2))
        assertEquals("ok", rule.compute(100))
    }
}