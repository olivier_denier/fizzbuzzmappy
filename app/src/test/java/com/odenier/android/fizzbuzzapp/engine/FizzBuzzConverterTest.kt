package com.odenier.android.fizzbuzzapp.engine

import org.junit.Assert.assertEquals
import org.junit.Test

class FizzBuzzConverterTest {

    @Test
    fun should_check_convert_method_return() {
        val converter = FizzBuzzConverter(100)

        assertEquals("", converter.convert(0))
        assertEquals("1", converter.convert(1))
        assertEquals("2", converter.convert(2))
        assertEquals("fizz", converter.convert(3))
        assertEquals("4", converter.convert(4))
        assertEquals("buzz", converter.convert(5))
        assertEquals("fizz", converter.convert(6))
        assertEquals("7", converter.convert(7))
        assertEquals("8", converter.convert(8))
        assertEquals("fizz", converter.convert(9))
        assertEquals("buzz", converter.convert(10))
        assertEquals("11", converter.convert(11))
        assertEquals("fizz", converter.convert(12))
        assertEquals("13", converter.convert(13))
        assertEquals("14", converter.convert(14))
        assertEquals("fizzbuzz", converter.convert(15))
        assertEquals("16", converter.convert(16))
    }

    @Test
    fun should_check_converter_result() {
        val converter = FizzBuzzConverter(16)
        val result = converter.generate()
        val formattedResult = StringBuilder()
        result.forEach { formattedResult.append(it+",") }
        formattedResult.deleteCharAt(formattedResult.length-1)

        assertEquals(16, result.size)
        assertEquals("1,2,fizz,4,buzz,fizz,7,8,fizz,buzz,11,fizz,13,14,fizzbuzz,16", formattedResult.toString())
    }
}