package com.odenier.android.fizzbuzzapp.ui.form

import androidx.databinding.Bindable
import androidx.databinding.library.baseAdapters.BR
import com.odenier.android.fizzbuzzapp.util.ObservableViewModel

class FormViewModel : ObservableViewModel() {

    var model = FormModel()

    @Bindable
    fun getFirstInt(): String {
        return if (model.firstInt > 0) model.firstInt.toString() else ""
    }

    fun setFirstInt(firstInt: String) {
        model.firstInt = try {
            firstInt.toInt()
        } catch (e : NumberFormatException) {
            0
        }
        notifyPropertyChanged(BR.firstInt)
    }

    @Bindable
    fun getFirstString(): String {
        return model.firstString
    }

    fun setFirstString(firstString: String) {
        model.firstString = firstString
        notifyPropertyChanged(BR.firstString)
    }

    @Bindable
    fun getSecondInt(): String {
        return if (model.secondInt > 0) model.secondInt.toString() else ""
    }

    fun setSecondInt(secondInt: String) {
        model.secondInt = try {
            secondInt.toInt()
        } catch (e : NumberFormatException) {
            0
        }
        notifyPropertyChanged(BR.secondInt)
    }

    @Bindable
    fun getSecondString(): String {
        return model.secondString
    }

    fun setSecondString(secondString: String) {
        model.secondString = secondString
        notifyPropertyChanged(BR.secondString)
    }

    @Bindable
    fun getLimit(): String {
        return if (model.limit > 0) model.limit.toString() else ""
    }

    fun setLimit(limit: String) {
        model.limit = try {
            limit.toInt()
        } catch (e : NumberFormatException) {
            0
        }
        notifyPropertyChanged(BR.limit)
    }

    fun isFormValid(): Boolean {
        // TODO Check each input data
        return true
    }
}