package com.odenier.android.fizzbuzzapp.ui.stat

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.odenier.android.fizzbuzzapp.R
import com.odenier.android.fizzbuzzapp.storage.RequestStorage
import com.odenier.android.fizzbuzzapp.ui.form.FormModel

class StatFragment : Fragment() {

    private var data = arrayListOf<Pair<String, Int>>()

    companion object {
        val TAG = StatFragment::class.simpleName

        @JvmStatic
        fun newInstance() = StatFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // TODO this task should be executed in async
        val requestArray = loadRequest()
        data = StatTransformer(requestArray).transform() as ArrayList<Pair<String, Int>>
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_list, container, false)

        // Set the adapter
        if (view is RecyclerView) {
            with(view) {
                layoutManager = LinearLayoutManager(context)
                adapter = StatRecyclerViewAdapter(data)
            }
        }
        return view
    }

    private fun loadRequest(): List<FormModel> {
        val storage = RequestStorage(requireContext())
        return storage.loadFile()
    }
}