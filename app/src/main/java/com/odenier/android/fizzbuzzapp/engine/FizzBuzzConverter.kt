package com.odenier.android.fizzbuzzapp.engine

const val FIZZ = "fizz"
const val BUZZ = "buzz"
class FizzBuzzConverter(private val limit: Int) {

    /**
     * The original fizz-buzz consists in writing all numbers from 1 to 100, and just replacing
     * all multiples of 3 by "fizz",
     * all multiples of 5 by "buzz",
     * and all multiples of 15 by "fizzbuzz".
     * The output would look like this: "1,2,fizz,4,buzz,fizz,7,8,fizz,buzz,11,fizz,13,14,fizzbuzz,16,...".
     */
    fun generate(): List<String> {
        if (!checkLimitValidity()) throw RuntimeException("limit not in range $MIN..$MAX, sorry!")

        val values = ArrayList<String>()

        for (number in MIN..limit) {
            values.add(convert(number))
        }

        return values
    }

    fun convert(number: Int): String {
        if (number < MIN) return ""

        val result = StringBuilder()
        if (number % 3 == 0) result.append(FIZZ)
        if (number % 5 == 0) result.append(BUZZ)
        if (result.isEmpty()) result.append(number)

        return result.toString()
    }

    private fun checkLimitValidity() = (limit in MIN until MAX)
}