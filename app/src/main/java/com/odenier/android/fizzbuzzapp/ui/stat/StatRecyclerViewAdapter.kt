package com.odenier.android.fizzbuzzapp.ui.stat

import android.content.Context
import android.util.DisplayMetrics
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.odenier.android.fizzbuzzapp.R


const val RIGHT_MARGIN = 60; // dpi
class StatRecyclerViewAdapter(private val values: List<Pair<String, Int>>) : RecyclerView.Adapter<StatRecyclerViewAdapter.ViewHolder>() {

    private var barViewMaxWidth = 0
    private var topCount = 0

    init {
        topCount = if (values.isNotEmpty()) values[0].second else 0
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.fragment_stat_item,
            parent,
            false
        )

        val displayMetrics: DisplayMetrics = parent.context.resources.displayMetrics
        barViewMaxWidth = displayMetrics.widthPixels - rightMargin(parent.context)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.labelTextView.text = item.first
        holder.barView.layoutParams.width = item.second * barViewMaxWidth / topCount
    }

    override fun getItemCount(): Int = values.size


    private fun rightMargin(context: Context): Int {
        val px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, RIGHT_MARGIN.toFloat(), context.resources.displayMetrics)
        return px.toInt()
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val labelTextView: TextView = view.findViewById(R.id.labelTextView)
        val barView: View = view.findViewById(R.id.barView)
    }

}