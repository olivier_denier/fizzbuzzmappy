package com.odenier.android.fizzbuzzapp.ui.stat

import android.util.Log
import com.odenier.android.fizzbuzzapp.ui.form.FormModel

class StatTransformer(private val requestArray: List<FormModel>) {

    private var map = HashMap<String, Int>()

    fun transform(): List<Pair<String, Int>> {
        // Count Pair(firstInt,secondInt) occurrences
        for (request in requestArray) {
            val key = "${request.firstInt}-${request.secondInt}"
            if (map.contains(key)) {
                map[key]?.inc()?.let { map.put(key, it) }
            } else {
                map[key] = 1
            }
        }

        // Descending sort
        val sortedMap = map.toList().sortedByDescending { (k, v) -> v }.toMap()

        // Set arrayList items defined by (firstInt,secondInt) key and count occurrence
        val data = arrayListOf<Pair<String, Int>>()
        sortedMap.forEach { (k, v) -> data.add(Pair(k, v)) }

        for (keyValue in data) {
            Log.i(StatFragment.TAG, "keyValue: ${keyValue.first}->${keyValue.second}")
        }

        return data
    }
}