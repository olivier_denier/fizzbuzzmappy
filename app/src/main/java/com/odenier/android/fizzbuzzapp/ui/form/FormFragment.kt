package com.odenier.android.fizzbuzzapp.ui.form

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.odenier.android.fizzbuzzapp.MainActivity
import com.odenier.android.fizzbuzzapp.R
import com.odenier.android.fizzbuzzapp.databinding.FragmentFormBinding
import com.odenier.android.fizzbuzzapp.storage.RequestStorage

class FormFragment : Fragment(R.layout.fragment_form) {

    companion object {
        val TAG = FormFragment::class.simpleName

        fun newInstance() = FormFragment()
    }

    private var _binding: FragmentFormBinding? = null
    // This property is only valid between onCreateView and onDestroyView.
    private val binding get() = _binding!!
    private lateinit var viewModel: FormViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(FormViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentFormBinding.inflate(inflater, container, false)
        binding.formViewModel = viewModel

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // TODO State Button (disabled) should be manage if input values are invalid
        binding.validFormButton.setOnClickListener {
            if (viewModel.isFormValid()) {
                saveRequestForm()
                displayList()
            }
        }

    }

    private fun saveRequestForm() {
        val storage = RequestStorage(requireContext())
        storage.saveFile(viewModel.model)
    }

    private fun displayList() {
        (activity as? MainActivity)?.displayList(viewModel.model)
    }

}