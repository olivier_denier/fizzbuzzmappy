package com.odenier.android.fizzbuzzapp.engine

class FizzBuzzRule(val intValue: Int, private val stringValue: String) {

    fun compute(number: Int) = if (intValue > 0 && number % intValue == 0) stringValue else ""
}