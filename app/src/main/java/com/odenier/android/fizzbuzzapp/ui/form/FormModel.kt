package com.odenier.android.fizzbuzzapp.ui.form

import android.os.Parcel
import android.os.Parcelable

data class FormModel(var firstInt:Int = 0, var firstString:String = "",
                     var secondInt:Int = 0, var secondString:String = "",
                     var limit:Int = 0) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString()!!,
        parcel.readInt(),
        parcel.readString()!!,
        parcel.readInt()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(firstInt)
        parcel.writeString(firstString)
        parcel.writeInt(secondInt)
        parcel.writeString(secondString)
        parcel.writeInt(limit)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<FormModel> {
        override fun createFromParcel(parcel: Parcel): FormModel {
            return FormModel(parcel)
        }

        override fun newArray(size: Int): Array<FormModel?> {
            return arrayOfNulls(size)
        }

        fun csvMarshall(formModel: FormModel): String {
            return "${formModel.firstInt};" +
                    "${formModel.firstString};" +
                    "${formModel.secondInt};" +
                    "${formModel.secondString};" +
                    "${formModel.limit};"
        }

        fun csvUnmarshall(data: String): FormModel {
            val dataArray = data.split(";")
            return FormModel(
                    dataArray[0].toInt(),
                    dataArray[1],
                    dataArray[2].toInt(),
                    dataArray[3],
                    dataArray[4].toInt())
        }
    }

    override fun toString(): String {
        return "$firstInt;$firstString;$secondInt;$secondString;$limit;"
    }
}