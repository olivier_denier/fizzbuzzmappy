package com.odenier.android.fizzbuzzapp

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.commit
import com.odenier.android.fizzbuzzapp.ui.form.FormFragment
import com.odenier.android.fizzbuzzapp.ui.form.FormModel
import com.odenier.android.fizzbuzzapp.ui.list.FizzBuzzListFragment
import com.odenier.android.fizzbuzzapp.ui.stat.StatFragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        displayFormFragment()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection
        return when (item.itemId) {
            R.id.form -> {
                displayFormFragment()
                true
            }
            R.id.stat -> {
                displayStatFragment()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun displayFormFragment() {
        supportFragmentManager.commit {
            setReorderingAllowed(true)
            addToBackStack(null)
            replace(R.id.fragment_container_view, FormFragment.newInstance(), FormFragment.TAG)
        }
    }

    fun displayList(formModel: FormModel) {
        supportFragmentManager.commit {
            setReorderingAllowed(true)
            addToBackStack(null)
            replace(R.id.fragment_container_view, FizzBuzzListFragment.newInstance(formModel), FormFragment.TAG)
        }
    }

    private fun displayStatFragment() {
        supportFragmentManager.commit {
            setReorderingAllowed(true)
            addToBackStack(null)
            replace(R.id.fragment_container_view, StatFragment.newInstance(), StatFragment.TAG)
        }
    }

}