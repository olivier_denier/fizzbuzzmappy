package com.odenier.android.fizzbuzzapp.ui.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.odenier.android.fizzbuzzapp.R
import com.odenier.android.fizzbuzzapp.engine.FizzBuzzRule
import com.odenier.android.fizzbuzzapp.engine.GenericFizzBuzzConverter
import com.odenier.android.fizzbuzzapp.ui.form.FormModel

class FizzBuzzListFragment : Fragment() {

    private lateinit var formModel: FormModel
    private var data = listOf<String>()

    companion object {
        val TAG = FizzBuzzListFragment::class.simpleName

        const val ARG_REQUEST_FORM = "ARG_REQUEST_FORM"

        @JvmStatic
        fun newInstance(formModel: FormModel) =
                FizzBuzzListFragment().apply {
                    arguments = Bundle().apply {
                        putParcelable(ARG_REQUEST_FORM, formModel)
                    }
                }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            formModel = it.getParcelable(ARG_REQUEST_FORM) ?: FormModel()
        }

        if (formModel.limit > 0) {
            // TODO this task should be executed in async
            val converter = GenericFizzBuzzConverter(formModel.limit)
            converter.addRule(FizzBuzzRule(formModel.firstInt, formModel.firstString))
            converter.addRule(FizzBuzzRule(formModel.secondInt, formModel.secondString))
            data = converter.generate()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_list, container, false)

        // Set the adapter
        if (view is RecyclerView) {
            with(view) {
                layoutManager = LinearLayoutManager(context)
                val dividerItemDecoration = DividerItemDecoration(requireContext(), (layoutManager as LinearLayoutManager).orientation)
                addItemDecoration(dividerItemDecoration)
                adapter = GenericStringRecyclerViewAdapter(data)
            }
        }
        return view
    }

}