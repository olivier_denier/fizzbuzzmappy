package com.odenier.android.fizzbuzzapp.engine

const val MIN = 1
const val MAX = 9999
class GenericFizzBuzzConverter(private val limit: Int) {

    var rules = mutableListOf<FizzBuzzRule>()
        private set

    fun generate(): List<String> {
        if (!checkLimitValidity()) throw RuntimeException("limit not in range $MIN..$MAX, sorry!")

        val values = ArrayList<String>()
        for (number in MIN..limit) {
            values.add(convert(number))
        }

        return values
    }

    fun convert(number: Int): String {
        if (number < MIN) return ""

        val result = StringBuilder()
        for (rule in rules) {
            result.append(rule.compute(number))
        }

        if (result.isEmpty()) result.append(number)

        return result.toString()
    }

    private fun checkLimitValidity() = (limit in MIN until MAX+1)

    fun addRule(rule: FizzBuzzRule) {
        // TODO check if this rule isn't already exist in rule array
        // What to do if rule with same intValue already exist ? replace or ignore ?
        // Define rules as a Map should be better to manage duplicate rule (maybe in v2 ;-))
        rules.add(rule)
    }

    fun getLimit() = limit
}