package com.odenier.android.fizzbuzzapp.storage

import android.content.Context
import android.util.Log
import com.odenier.android.fizzbuzzapp.ui.form.FormModel
import java.io.File
import java.lang.ref.WeakReference


const val REQUEST_FILENAME = "request.csv"
class RequestStorage(context: Context) : IFileStorage<FormModel> {
    private val weakContext = WeakReference(context)

    companion object {
        private val TAG = RequestStorage::class.simpleName
    }

    /**
     * Method used to load all the request input form
     * Each line represents a formModel formatted in CSV with separator ";"
     * @return FormModels' list
     */
    override fun loadFile(): List<FormModel> {
        val requestArray = arrayListOf<FormModel>()

        val file = File(weakContext.get()?.filesDir, REQUEST_FILENAME)
        if (file.exists() && file.canRead()) {
            file.bufferedReader().useLines {
                for (line in it.iterator()) {
                    val formModel = FormModel.csvUnmarshall(line)
                    requestArray.add(formModel)
                }
            }
        }

        Log.d(TAG, "requestArray=$requestArray")
        return requestArray
    }

    /**
     * Method used to store in File the request input form
     * Each formModel is converted into CSV and saved in file
     */
    override fun saveFile(data: FormModel) {
        val csv = FormModel.csvMarshall(data)
        Log.d(TAG, "csv=$csv")
        weakContext.get()?.openFileOutput(REQUEST_FILENAME, Context.MODE_APPEND)
                ?.bufferedWriter()?.use {
                    it.append(csv)
                    it.newLine()
                    it.close()
                }
    }

}