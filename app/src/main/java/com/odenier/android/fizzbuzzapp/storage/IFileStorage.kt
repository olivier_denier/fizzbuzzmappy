package com.odenier.android.fizzbuzzapp.storage

interface IFileStorage<T> {

    fun loadFile(): List<T>
    fun saveFile(data: T)
}